# Documentation static site generator & deployment tool
mkdocs>=1.1.2
mkdocs-glightbox>=0.1.4
mkdocs-encriptmail-plugin>=0.9.7
#Quelle: https://stackoverflow.com/questions/11783875/importerror-no-module-named-bs4-beautifulsoup
BeautifulSoup4

# Add your custom theme if not inside a theme_dir
# (https://github.com/mkdocs/mkdocs/wiki/MkDocs-Themes)
#  mkdocs-material>=5.4.0
