# Der Stamm Ansgar
Wir, der Stamm Ansgar, sind Teil des Verbandes Christlicher Pfadfinderinnen und Pfadfinder ([VCP](https://vcp.de)).
Seit dem Jahr 1988, unserem Gründungsjahr, wurden bereits viele Veranstaltungen geplant und in die Tat umgesetzt.

Die Veranstaltungen des Stammes werden von der Führungsrunde geleitet, die sich einmal die Woche trifft um alles zu besprechen, was den Stamm betrifft.

## aktuelle Gruppen
* Königstieger &bull; Kinderstufe &bull; um die 10J &bull; Donnerstags 17°°-18°° 
* Erdmänchen &bull; Pfadfinderstufe &bull; um die 14J &bull; Mittwochs 18°°-19°°
* Führungsrunde &bull; Donnerstags 20°°

## aktuelle Ämter
* Stammesführung: Vorname Nachname &bull; Vorname Nachname &bull; <a href="javascript:linkTo_UnCryptMailto(hierIstEinKomplizierterHaswert);">ungueltigeEmail.de</a>
* Materialwart: Vorname Nachname &bull; Vorname Nachname &bull; <a mailto="ungueltigeEmail.de">ungueltigeEmail.de</a>

## Wer war eigentlich Ansgar?
Schon 1987 informierte sich Pfarrer Helmut Gross bei der [Landeszentrale](https://hessen.vcp.de) in Bad Nauheim über den [VCP](https://vcp.de), um dann wenig später zusammen mit Margit Dries und Christoph Franke eine erste Gruppe aufzumachen. Sie setzte sich aus den damaligen Konfirmanden zusammen und bestand bis zum Winter 1987/88.
Nach den Sommerferien '87 wurde eine neue Gruppe gegründet, die bald schon mehr als 20 Mitglieder zählte. Da sonst keiner vorher was mit Pfadfinderei zu tun hatte, außer Herrn Pfarrer Gross, war es anfangs schwierig den neuen Mitgliedern Geist und Traditionen des VCP näher zu bringen. Nach mehreren Lagern aber setzte sich langsam der Geist des VCP durch. 1990 wurde unser Stamm aufgenommen.
Heute sind 69 Mitglieder in unserem Stamm aktiv. Wir haben fünf Sippen und zwei Meuten, die sich einmal in der Woche treffen. Donnerstags trifft sich die Führungsrunde mit etwa 15 Teilnehmern. Sie setzt sich zusammen aus allen Sippenführern und den Stammesführern.

1990 fand in Noer an der Ostsee ein großes Landeslager statt, zu dem auch der damals noch relativ junge Stamm hinfuhr.  Damals gab es verschiedene Freizeitangebote für die Stämme, unter anderem der Besuch in dem ehemaligen Wikingerhafen Haithabu. Dort stieß man auf den Namen Ansgar. [Ansgar](https://de.wikipedia.org/wiki/Ansgar_von_Bremen) war ein Mönch, der das Evangelium in Dänemark verkünden sollte im Auftrag von Ludwig dem Frommen. In Dänemark begann er seine Mission, indem er Jugendliche unterrichtete. Überall, wo er auf seinen Missionsreisen hinkam, ob Oslo oder Hamburg sammelte er Jugendliche um sich und gründete Schulen, um sie für Christus zu gewinnen. 

## Beiträge und Anmeldungen ? 
Jeder ist immer herzlich eingeladen einfach mal vorbei zu schauen ob es was für einen ist. 
Wenn man dan dazustoßen möchte brauchen wir dann zumindest Kontaktdaten. Da Pfadinderei sich aber nicht nur hier abspielt sondern überall sind wir in Verbänden Organisiert. 
Die Anmeldung beim VCP selber empfehlen wir nach einem Jahr. Diese geht mitlerweile auch [Papier los.](https://vcp.gruen.net/app_mitgliedwerden) <sup> Land 500000 Stamm 504009</sup>

# &Dfr;&ifr;&efr; &Cfr;&hfr;&rfr;&ofr;&nfr;&ifr;&kfr;&efr;&nfr; &vfr;&ofr;&nfr; &Afr;&nfr;&sfr;&gfr;&afr;&rfr;

## 20er
#### 2022
* Pfaditag 22
* Jugendkirchentag in *Gernsheim*
* Regionspfingstlager **Mittelalter**
* Gruppe Königstieger ensteht
#### 2021
* Pfaditag 21
#### 2020
* Corona
## 10er
#### 2019
* Jugendaustausch Prösen in *Röderland*
* Evangelischer Kirchentag in *Dortmund*
#### 2018
* Jugendaustausch Prösen in *Hünfelden*
* Landeslager *Hanse*
* Jugendkirchentag in *Weilburg*
#### 2017
* Jugendaustausch Prösen in *Röderland*
* Evangelischer Kirchentag in *Berlin*
* Regionspfingstlager 
#### 2016
* Jugendaustausch Prösen in *Hünfelden*
* Jugendkirchentag in *Offenbach*
#### 2015
* Jugendaustausch Prösen in *Röderland*
* Evangelischer Kirchentag in *Stuttgart*
* Repfila *Götter*
#### 2014
* Jugendaustausch Prösen in *Hünfelden*
* Pfingstlager mit mehreren Stämmen zum Thema **Asterix und Obelix**
#### 2013
* Jugendaustausch Prösen in *Röderland*
* 25 Jahre Jubiläum
* Evangelischer Kirchentag in *Hamburg*
* Regionspfingstlager *Piraten*
#### 2012
* Herbstlager **Mit Käptai'n Blaubär auf See**
* Jugendaustausch Prösen in *Hünfelden*
* Regionsvolleyballtunier in *Dauborn*
* Pfingstlager **der kleine Hobbit**
#### 2011
* Sommerlager in Prösen *Start des Jugendaustausch*
* Evangelischer Kirchentag in *Dresden*
* Regionspfingstlager Märchen
#### 2010
* Bundeslager **Leinen Los** Almke
    * [Lagerzeitung lesen](https://issuu.com/vcp.de/stacks/6c817eb267854599b5c4be0dfa3dd38e) <sup>issuu.com</sup>
* Ökumenischer Kirchenteg in *München*
* Jugendkirchentag in *Mainz*
## 00er
#### 2009
* Evangelischer Kirchentag in *Bremen*
* Regionspfingstlager **Fragil**
#### 2008
* Landeslager **Lampenfieber**
    * [Abschluss Abend Theater Auschnitte](https://vimeo.com/1441751) <sup>Vimeo</sup>
* 20 Jahre Jubiläum 
* Pfingstlager **auf in den Süden** mit Fischbach
#### 2007
* Herbstlager **Hexen**
* 100 Jahre Pfadfinder Feier in Belrin / und auf Schloss Belvue
* Sippe des Jahrhundert geht nach Dauborn
* Landespfingtlager **Beisen die** mit dem BdP
* Evangelischer Kirchentag in *Köln*
#### 2006
* Herbstlager 
* Bauwagen Abgebrannt
* Bundeslager **100pro** *mit Partnerstamm Talita Kumi* 
    * [Lagerzeitung lesen](https://issuu.com/vcp.de/stacks/7832ca67d98b444288ae7e3dcec83baf) <sup>issuu.com</sup>
* Pfingslager **Zirkus**
* neue meute

#### 2005
* Spielplatz umbau unterstützung
* Führu Irland Fahrt
* Gabel eines Bruders nach 4 Jahren auf der Schmidtburg Ruine gefunden
* Regionspfingstlager **Sintie und Roma** *Wildpark Großgerau*
#### 2004
* Landeslager **Thyngdal** 
    * [Lagerlied](https://www.youtube.com/watch?v=XNynEfI3wA8) <sup>YouTube</sup>
#### 2003
* Sommerlager Eiszeit Niedersonthofen am See 
* Stammes Jubiläum 15 Jahre
* Pfingstlager Homberg Ohm
* Schneefüchse entstehen
#### 2002
* Regionspfingstlager **Chinalager**
#### 2001
* Landeslager **A Scout Odysee** mit Weisrussichen Gästen
* Pfingstlager **Zauberwald**  Mit Gabel Verlusst
#### 2000
* Sommerlager **Auf der Suche nach dem Drachen der Weisheit** Elsas
* Regionspfingstlager **1000 und 1 Nacht**
* Meute Waschbären entsteht
## 90er
#### 1999
* Meute Trolle entsteht
#### 1998
* Bundeslager **und sie dreht sich Doch** Rheinsberg
* 10 Jahres Jubiläum
* Regionspfingstlager an der Mosel
* Meute Waldgesiter entshtet
#### 1997
* Landeslager **Sahne** in Tschechien 
* Pfila **Wallhala** Hirtenstein
* Fahrradtour nach Emden 
* Meute Waldgeister entsteht
#### 1996
* Sommerager **Seeräuber** auf Rügen
* Pfila *Steinzeit* mit Gelnhausen und Wächtersbach
* Löwen enstehen 
#### 1995
* Regionspfingstlager **Mittelalter** *Wildpark Großgerau*
* Meute Krokos entsteht
* Sippe Wiesel entsteht
#### 1993
* Stand auf dem Daubornermarkt
* Regionspfingstlager *Wirrberg*
#### 1992
* Pfaditag **Der 30 Jährige Krieg**
#### 1991
* Regionspfingstlager 
* Sippe Grizzlies entstet
#### 1990
* Landeslager **Seestern**
* Regionshaik um *Gnadentahl*
## 80er
#### 1989
* Sippe Glühwürmchen entsteht
#### 1988
* erste Freizeit in Eutersee
