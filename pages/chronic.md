# Stammes Chronic
## 20er
#### 2022
* Pfaditag 22
* Jugendkirchentag in *Gernsheim*
* Regionspfingstlager **Mittelalter**
* Gruppe Königstieger ensteht
#### 2021
* Pfaditag 21
## 10er
#### 2019
* Jugendaustausch Prösen in *Röderland*
* Evangelischer Kirchentag in *Dortmund*
#### 2018
* Jugendaustausch Prösen in *Hünfelden*
* Landeslager *Hanse*
* Jugendkirchentag in *Weilburg*
#### 2017
* Jugendaustausch Prösen in *Röderland*
* Evangelischer Kirchentag in *Berlin*
* Regionspfingstlager 
#### 2016
* Jugendaustausch Prösen in *Hünfelden*
* Jugendkirchentag in *Offenbach*
#### 2015
* Jugendaustausch Prösen in *Röderland*
* Evangelischer Kirchentag in *Stuttgart*
* Repfila *Götter*
#### 2014
* Jugendaustausch Prösen in *Hünfelden*
* Pfingstlager mit mehreren Stämmen zum Thema **Asterix und Obelix**
#### 2013
* Jugendaustausch Prösen in *Röderland*
* 25 Jahre Jubiläum
* Evangelischer Kirchentag in *Hamburg*
* Regionspfingstlager *Piraten*
#### 2012
* Herbstlager **Mit Käptai'n Blaubär auf See**
* Jugendaustausch Prösen in *Hünfelden*
* Regionsvolleyballtunier in *Dauborn*
* Pfingstlager **der kleine Hobbit**
#### 2011
* Sommerlager in Prösen *Start des Jugendaustausch*
* Evangelischer Kirchentag in *Dresden*
* Regionspfingstlager Märchen
#### 2010
* Bundeslager **Leinen Los** Almke
* Ökumenischer Kirchenteg in *München*
* Jugendkirchentag in *Mainz*
## 00er
#### 2009
* Evangelischer Kirchentag in *Bremen*
* Regionspfingstlager **Fragil**
#### 2008
* Landeslager **Lampenfieber**
* 20 Jahre Jubiläum 
* Pfingstlager **auf in den Süden** mit Fischbach
#### 2007
* Herbstlager **Hexen**
* 100 Jahre Pfadfinder Feier in Belrin / und auf Schloss Belvue
* Sippe des Jahrhundert geht nach Dauborn
* Landespfingtlager **Beisen die** mit dem BdP
* Evangelischer Kirchentag in *Köln*
#### 2006
* Herbstlager 
* Bauwagen Abgebrannt
* Bundeslager **100pro** *mit Partnerstamm Talita Kumi* 
* Pfingslager **Zirkus**
* neue meute

#### 2005
* Spielplatz umbau unterstützung
* Führu Irland Fahrt
* Gabel eines Bruders nach 4 Jahren auf der Schmidtburg Ruine gefunden
* Regionspfingstlager **Sintie und Roma** *Wildpark Großgerau*
#### 2004
* Landeslager **Thyngdal** 
#### 2003
* Sommerlager Eiszeit Niedersonthofen am See 
* Stammes Jubiläum 15 Jahre
* Pfingstlager Homberg Ohm
* Schneefüchse entstehen
#### 2002
* Regionspfingstlager **Chinalager**
#### 2001
* Landeslager **A Scout Odysee** mit Weisrussichen Gästen
* Pfingstlager **Zauberwald**  Mit Gabel Verlusst
#### 2000
* Sommerlager **Auf der Suche nach dem Drachen der Weisheit** Elsas
* Regionspfingstlager **1000 und 1 Nacht**
* Meute Waschbären entsteht
## 90er
#### 1999
* Meute Trolle entsteht
#### 1998
* Bundeslager **und sie dreht sich Doch** Rheinsberg
* 10 Jahres Jubiläum
* Regionspfingstlager an der Mosel
* Meute Waldgesiter entshtet
#### 1997
* Landeslager **Sahne** in Tschechien 
* Pfila **Wallhala** Hirtenstein
* Fahrradtour nach Emden 
* Meute Waldgeister entsteht
#### 1996
* Sommerager **Seeräuber** auf Rügen
* Pfila *Steinzeit* mit Gelnhausen und Wächtersbach
* Löwen enstehen 
#### 1995
* Regionspfingstlager **Mittelalter** *Wildpark Großgerau*
* Meute Krokos entsteht
* Sippe Wiesel entsteht
#### 1993
* Stand auf dem Daubornermarkt
* Regionspfingstlager *Wirrberg*
#### 1992
* Pfaditag **Der 30 Jährige Krieg**
#### 1991
* Regionspfingstlager 
* Sippe Grizzlies entstet
#### 1990
* Landeslager **Seestern**
* Regionshaik um *Gnadentahl*
## 80er
#### 1989
* Sippe Glühwürmchen entsteht
#### 1988
* erste Freizeit in Eutersee
