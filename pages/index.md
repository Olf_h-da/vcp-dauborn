![Pfaditag 2021](./img/PfaditagNacht21.jpg)
# Herzlich willkommen 
...auf einer Beispielseite eines Stammes vom [VCP](https://vcp.de)!

**Dies ist nicht die offizielle Webseite des VCP Dauborn! Bitte besuchen sie [https://VCP-Dauborn.de](https://VCP-Dauborn.de)**

Wir freuen uns sehr, dass ihr euch für unsere Arbeit interessiert und laden euch ein, auch einmal zu unseren Gruppenstunden zu kommen, um uns persönlich kennenzulernen. Schaut euch gründlich auf der Homepage um.

Die Grupenstunden Zeiten und andere Veranstalltungen sind [hier](./kalender) unter Termine zu finden

# Aktuells:

## Spamschutz durch Plugin
Das Plugin [encriptmail](https://github.com/Rolfff/mkdocs-encriptmail-plugin/) verschlüsselt "mailto:"-Links mit einem einfachen [Cäsar Algorithmus](https://de.wikipedia.org/wiki/Caesar-Verschl%C3%BCsselung) damit Spam-Crawler die Email-Addressen nicht einfach auslesen können. Durch einen klick des Webeiten-Besuchers werden die Adressen über JAvaScript wieder in ein "mailto:"-Link übersetzt und vom Browser an das Betriebsystem weitergegeben.   
* [test@test.test](mailto:test@test.test)  
* [te-st@te-st.te-st](mailto:te-st@te-st.te-st)  

## Neue Webseite
Seit gestern haben wir diese wunderschöne neue Webseite. Schaut euch um und teilt sie weiter.
Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.

## Bagira Stammes Pflanze
Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.

## Neue Meute 
Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.
