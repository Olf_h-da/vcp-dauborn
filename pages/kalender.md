
# Gruppenstunden
Die Gruppenstunden finden wöchentlich während der Schulzeit im oder am [ev. Gemeindehaus](https://goo.gl/maps/oGdBpLwaGfuFEC2N7 "Link zu Google Maps") in Dauborn statt.

* Königstieger &bull; Kinderstufe &bull; um die 10J &bull; Donnerstags 17°°-18°°
* Erdmänchen &bull; Pfadfinderstufe  &bull; um die 14J &bull; Mittwochs 18°°-19°°

# Weitere Kalender
Zuätzlich zu den Gruppenstunden finden unregeläßige weitere Planugs-, Arbeits- oder Gremientreffen statt. Unten auf dieser Seite sind unsere aktuell geplanten Lager und Veranstalltungen aufgelistet. Über die gelben Bereiche können Veranstalltungen von unseren weiteren Ebenen an- oder abgeschaltet werden.

ungefähre Altersinformationen bei den folgenden Veranstalltungen:

![Altersstufen](./img/stufen.png)

<noscript>
    <div class="frame frame-p20 bg-beige">
        <p>Die Termine werden über Java-Script von deinem Browser geladen. Deshalb solltest du Java-Script im Browser erlauben um die Termine sehen zu können.</p>
    </div>
    <br>
</noscript>

<div id="snk-datenschutz" class="frame frame-p20 bg-beige"  > 
    <div class="inner">
        <div class="vcpwlz text-vcpblau" style="font-size: 4em;">z</div>
        <p>Erst wenn du auf den Button klickst, werden die Termine von <a target="_blank" href="https://www.scoutnet.de/">ScoutNet</a> nachgeladen. Deine IP-Adresse wird dabei an externe Server von <a target="_blank" href="https://www.scoutnet.de/">ScoutNet</a> übertragen. Über den Datenschutz dieses Anbieters kannst du dich auf der Seite von <a target="_blank" href="https://www.scoutnet.de/">ScoutNet</a> informieren.</p>
        <div class="more-button" onclick="getAboLink();main();">Ein Klick für den Datenschutz</div>
    </div>
</div>

<form method="post" id="snk-form" >
    <input type="hidden" name="cIDs" id="snk-cIDs" value="">
</form>

<div id="snk-stammesAuswahl" class="snk-stammesAuswahl"></div>

## Terminliste
<div id="snk-termine" class="snk-termine"> </div>
<div class="snk-footer">
    <div class="snk-powered-by">
        Powered By&nbsp;<span><a target="_top" href="https://www.scoutnet.de/kalender/">ScoutNet.DE</a></span>
    </div>
</div>

## Kalender abonnieren
<ul id="snk-abo" class="snk-abo"> </ul>

### Anleitungen
* [Outlook](https://support.microsoft.com/de-de/office/importieren-oder-abonnieren-eines-kalenders-in-outlook-com-cff1429c-5af6-41ec-a5b4-74f2c278e98c)
* [Google Kalernder](https://support.google.com/calendar/answer/37095?hl=de)
* [Apple Kalender](https://support.apple.com/de-de/HT202361)
* [Mozilla Thunderbird](https://support.mozilla.org/de/kb/neue-kalender-erstellen)



<script type="text/javascript">
let mainOrganisationID = 2505;
let aktivCalendars = [mainOrganisationID,2370]
let alternativOrganisationIDs = [1273,2251,2370];
let organisationLogos = {
  1273: '../img/calendar/VCP_Bund_Kalender.png',
  2251: '../img/calendar/VCP_Land_Kalender.png',
  1165: '../img/calendar/vcp_region_kurhessen.png',
  778: '../img/calendar/vcp_region_main_kinzig.png',
  749: '../img/calendar/vcp_region_starkenburg.png',
  1116: '../img/calendar/vcp_region_wetterau.png',
  2370: '../img/calendar/vcp_region_rheinmain.png',
  2505: '../img/favicon.gif'
};
let calendarItems = Array();
let organisations = Array();
let terminAbo = document.getElementById('snk-abo');
let terminDatenschutz = document.getElementById('snk-datenschutz');
let terminTable = document.getElementById('snk-termine');
let terminForm = document.getElementById('snk-stammesAuswahl');

function getAboLink(){
    let link = "webcal://kalender.scoutnet.de/2.0/show.php?ssid=" + mainOrganisationID;
    for (const calendar of aktivCalendars) {
        if (calendar != mainOrganisationID) {
            link = link + "&addids[]=" + calendar;
        }
    }
    link = link + "&template=export/iCal.tpl&charset=utf8";

    let li= document.createElement("li");
    let p= document.createElement("p");
    p.setAttribute("class", "text-justify");
    let p_a = document.createElement("a")
    p_a.setAttribute("target", "_blank");
    p_a.setAttribute("href", link);
    let p_a_content = document.createTextNode("Hier aktuellen Kalender als ICal abonnieren!");
    p_a.appendChild(p_a_content);
    p.appendChild(p_a);
    li.appendChild(p);
    terminAbo.replaceChildren(li);





}

function snk_show_termin( termin_id, link ){
    var details = document.getElementById('snk-termin-'+termin_id);
    if(!this.putstyle_w3c){
        this.putstyle_w3c = 'table-row';
    }
    this.putstyle_ie = 'block';
    if( details.style.display == this.putstyle_w3c || details.style.display == this.putstyle_ie ){
        details.style.display = 'none';
        link.className = link.old_class;
    } else {
        try{
            details.style.display = this.putstyle_w3c;
        }catch(e){
            details.style.display = this.putstyle_ie;
        }
        link.old_class = link.className;
        link.className = 'snk-termin-link-opened';
    }
    return false;
}

async function fetchScoutnetData(url) {
     const response = await fetch(url, {
       method: 'GET', // *GET, POST, PUT, DELETE, etc.
    })
    if (!response.ok) {
        const message = `An error has occured: ${response.status}`;
        throw new Error(message);
    }
    return await response.json();
}

async function fetchAllData(){
    if (alternativOrganisationIDs.length != 0){
		try{
			const data = await fetchScoutnetData('https://api.scoutnet.de/api/0.2/groups/?json=[['+mainOrganisationID+','+alternativOrganisationIDs+']]');
			//console.log(data.elements)
			organisations = data.elements
		} catch(e){
			throw e;
		}
    }
    let allCalendars = alternativOrganisationIDs.concat(mainOrganisationID);
    for (const calendar of allCalendars) {
		try{
			const data = await fetchScoutnetData('https://api.scoutnet.de/api/0.2/group/'+calendar+'/events/');
			calendarItems = calendarItems.concat(data.elements);
		} catch(e){
			throw e;
		}
        
    }
    
    

    let today = new Date();
    today = today.toISOString().split('T')[0];
    unixToday = Math.floor(new Date(today).getTime() / 1000);
    
    calendarItems = calendarItems.filter(function(item) {
        let unixtime = 0;
        if (item.end_date != null) {
            unixtime = Math.floor(new Date(item.end_date).getTime() / 1000)
        } else {
            unixtime = Math.floor(new Date(item.start_date).getTime() / 1000)
        }
        return (unixtime >= unixToday)
    });


    calendarItems = calendarItems.sort((objA, objB) => {
        let Atime = objA.start_time;
        let Btime = objB.start_time
        if(objA.start_time == null){ Atime = "00:00:00"}
        if(objB.start_time == null){ Btime = "00:00:00"}
        return Math.floor(new Date(objA.start_date+"T"+Atime).getTime() / 1000) -
        Math.floor(new Date(objB.start_date+"T"+Btime).getTime() / 1000)
    });
}

function buildMonthRow(date){
    var options = { month: 'long' , year: 'numeric'};
    var stringdate = new Date(date).toLocaleDateString("de-DE", options);
    let tr_month = document.createElement("tr")
    tr_month.setAttribute("class", "snk-monat-row");
    let th = document.createElement("th")
    th.setAttribute("colspan", "6");
    th.setAttribute("class", "snk-monat-heading");
    let th_content = document.createTextNode(stringdate);
    th.appendChild(th_content);
    tr_month.appendChild(th);
    return tr_month;
}

function includes(list, search){
	for (var elem in list) {
		if(list[elem] == search){
			return true;
		}
	}
	return false;
}

function buildEventRow(event,number){
	let tr_event = document.createElement("tr");
	if (number % 2 == 0){
		tr_event.setAttribute("class", "bg-beige");
	}
	let td_event_img = document.createElement("td");
	td_event_img.setAttribute("class", "snk-eintrag-ebene");
    organisation = organisations.find(e => parseInt(e.global_id) == parseInt(event.group_id));
    if(typeof organisationLogos[parseInt(event.group_id)] === "undefined") {
        console.log(organisation.name+" Logo is undefined!");
    } else {
	    let td_img = document.createElement("img");
	    td_img.setAttribute("title", "Termin von " + organisation.name);
	    td_img.setAttribute("alt", "Logo von " + organisation.name);
        td_img.setAttribute("src", organisationLogos[parseInt(event.group_id)]);
	    td_img.setAttribute("height", "20");
	    td_event_img.appendChild(td_img);
    }
	tr_event.appendChild(td_event_img);
	
	let td_event_date = document.createElement("td")
	td_event_date.setAttribute("class", "snk-eintrag-datum");
	var options = { weekday: 'short', day: 'numeric', month: 'numeric'  };
	var startdate = new Date(event.start_date).toLocaleDateString("de-DE", options);
    var enddate = new Date(event.end_date).toLocaleDateString("de-DE", options);
	let td_event_date_content = null;
	if (event.start_date == event.end_date || event.end_date == null) {
		td_event_date_content = document.createTextNode(startdate);
		} else {
		td_event_date_content = document.createTextNode(startdate +" - " + enddate);
		}
    td_event_date.appendChild(td_event_date_content);
	tr_event.appendChild(td_event_date);
	
	
	let td_event_time = document.createElement("td");
	td_event_time.setAttribute("class", "snk-eintrag-zeit");
	if (event.start_time){
		var starttime = event.start_time.split(':');
        let td_event_time_content = "";
        if (event.end_time){
		    var endtime = event.end_time.split(':');
		    td_event_time_content = document.createTextNode(starttime[0]+":"+starttime[1]+" - " + endtime[0]+":"+endtime[1]);
        } else {
            td_event_time_content = document.createTextNode(starttime[0]+":"+starttime[1]);
        }
		td_event_time.appendChild(td_event_time_content);
	}
	tr_event.appendChild(td_event_time);
	
	let td_event_title = document.createElement("td")
	td_event_title.setAttribute("class", "snk-eintrag-titel");
        if (event.description || event.zip || event.location || event.url_text || event.url ){
            let td_event_title_a = document.createElement("a")
            td_event_title_a.setAttribute("class", "snk-termin-link");
            td_event_title_a.setAttribute("onclick", "if(snk_show_termin) return snk_show_termin("+event.id+",this);");
            td_event_title_a.setAttribute("name", "snk-termin-"+event.id);
            td_event_title_a.setAttribute("href", "#snk-termin-"+event.id);
            let td_event_title_a_content = document.createTextNode(event.title);
            td_event_title_a.appendChild(td_event_title_a_content);
            td_event_title.appendChild(td_event_title_a);
        } else {
            let td_event_title_content = document.createTextNode(event.title);
            td_event_title.appendChild(td_event_title_content);
        }
	tr_event.appendChild(td_event_title);
	

	let td_event_stufe = document.createElement("td")
	td_event_stufe.setAttribute("class", "snk-eintrag-stufe");
	if (includes(event.keywords.elements, "Leiter")){
		let td_stufe = document.createElement("img")
		td_stufe.setAttribute("title", "Leiter");
		td_stufe.setAttribute("alt", "Leiter");
		td_stufe.setAttribute("src", "../img/calendar/Erw.gif");
		td_event_stufe.appendChild(td_stufe);
	}
	if (includes(event.keywords.elements, "Ranger/Rover")){
		let td_stufe = document.createElement("img")
		td_stufe.setAttribute("title", "Ranger/Rover");
		td_stufe.setAttribute("alt", "Ranger/Rover");
		td_stufe.setAttribute("src", "../img/calendar/RR.gif");
		td_event_stufe.appendChild(td_stufe);
	}
	if (includes(event.keywords.elements, "Pfadfinder")){
		let td_stufe = document.createElement("img")
		td_stufe.setAttribute("title", "Pfadfinder");
		td_stufe.setAttribute("alt", "Pfadfinder");
		td_stufe.setAttribute("src", "../img/calendar/PP.gif");
		td_event_stufe.appendChild(td_stufe);
	}
	if (includes(event.keywords.elements, "Jungpfadfinder")){
		let td_stufe = document.createElement("img")
		td_stufe.setAttribute("title", "Jungpfadfinder");
		td_stufe.setAttribute("alt", "Jungpfadfinder");
		td_stufe.setAttribute("src", "../img/calendar/JuP.gif");
		td_event_stufe.appendChild(td_stufe);
	}
	if (includes(event.keywords.elements, "Kinderstufe")){
		let td_stufe = document.createElement("img")
		td_stufe.setAttribute("title", "Kinderstufe");
		td_stufe.setAttribute("alt", "Kinderstufe");
		td_stufe.setAttribute("src", "../img/calendar/KiP.gif");
		td_event_stufe.appendChild(td_stufe);
	}
	tr_event.appendChild(td_event_stufe);

    return tr_event;
}

function buildEventDetailRow(event,number){
    let tr_detail = document.createElement("tr")
    tr_detail.setAttribute("id", "snk-termin-"+event.id); 
    if (number % 2 == 0){
        tr_detail.setAttribute("class", "snk-termin-infos bg-beige");
    } else {
        tr_detail.setAttribute("class", "snk-termin-infos"); 
    }
    tr_detail.setAttribute("style", ""); 
    let td_detail = document.createElement("td")
    td_detail.setAttribute("colspan", "6");
    let dl_detail = document.createElement("dl")
    if (event.description){
        let dt_detail = document.createElement("dt");
        dt_detail.setAttribute("class", "snk-eintrag-beschreibung");
        let dt_content = document.createTextNode("Beschreibung");
        dt_detail.appendChild(dt_content);
        dl_detail.appendChild(dt_detail);
        let dd_detail = document.createElement("dd");
        let dd_content = document.createTextNode(event.description);
        dd_detail.appendChild(dd_content);
        dl_detail.appendChild(dd_detail);
    }

    if (event.url){
        let dt_url = document.createElement("dt");
        dt_url.setAttribute("class", "snk-eintrag-url-label");
        let dt_url_content = document.createTextNode("Link");
        dt_url.appendChild(dt_url_content );
        dl_detail.appendChild(dt_url);
        let dd_url = document.createElement("dd");
        dd_url.setAttribute("class", "snk-eintrag-url");
        let dd_url_content = document.createElement("a")
        dd_url_content.setAttribute("target", "_blank");
        if (event.url.startsWith('http')) {
            dd_url_content.setAttribute("href", event.url);
        } else {
            dd_url_content.setAttribute("href", "https://"+event.url);
        }
        let dd_url_content_content = document.createTextNode(event.url_text);
        if(!event.url_text){
            dd_url_content_content = document.createTextNode(event.url);
        } 
        dd_url_content.appendChild(dd_url_content_content);
        dd_url.appendChild(dd_url_content);
        dl_detail.appendChild(dd_url); 
    }

    if (event.zip || event.location){
        let dt_ort = document.createElement("dt");
        dt_ort.setAttribute("class", "snk-eintrag-ort");
        let dt_ort_content = document.createTextNode("Ort");
        dt_ort.appendChild(dt_ort_content );
        dl_detail.appendChild(dt_ort);
        let dd_ort = document.createElement("dd");
        let dd_ort_content = document.createTextNode(event.zip + " " + event.location);
        dd_ort.appendChild(dd_ort_content );
        dl_detail.appendChild(dd_ort);
    }
    

	
    td_detail.appendChild(dl_detail);
    tr_detail.appendChild(td_detail);
    return tr_detail;
}

function buildList(){
    tableHeader = ["Ebene", "Datum", "Zeit", "Titel", "Stufe"];
    let tr = document.createElement("tr");
    tr.setAttribute("class", "snk-headings-row");
    //Create Header
    for (const element of tableHeader) {
        let th = document.createElement("th")
		th.setAttribute("class", "snk-eintrag-"+element);
        let th_content = document.createTextNode(element);
        th.appendChild(th_content);
        tr.appendChild(th);
    }
    let tbody = document.createElement("tbody")
    tbody.appendChild(tr)    

	
    let i = 1;
    var options = { month: 'numeric' };
    var oldMonth = 0
    let aktivCalendarItems = calendarItems.filter(item  => { return (aktivCalendars.includes(parseInt(item.group_id)))});
	for (const calendarItem of aktivCalendarItems) {
        
        var month = new Date(calendarItem.start_date).toLocaleDateString("de-DE", options);
        if (oldMonth != month){
            tbody.appendChild(buildMonthRow(calendarItem.start_date));
            oldMonth = month;
        }
		tbody.appendChild(buildEventRow(calendarItem,i));
        tbody.appendChild(buildEventDetailRow(calendarItem,i));
        i++;
	}

    let table = document.createElement("table");
    table.appendChild(tbody)

    terminTable.replaceChildren(table);
}


function toggelSelect(calendarID){
    if ( aktivCalendars.includes(parseInt(calendarID))){
        aktivCalendars = aktivCalendars.filter(item  => { return (item != parseInt(calendarID) )});
        buildCalendarForm();
        buildList();
    } else {
        aktivCalendars = aktivCalendars.concat(parseInt(calendarID));

        buildCalendarForm();
        buildList();
    }
    getAboLink();
}


function buildCalendarForm(){
    let cIDs = document.getElementById('snk-cIDs').value;
    if ( cIDs != ""){
        aktivCalendars = cIDs.split(",");
        aktivCalendars = aktivCalendars.concat(mainOrganisationID)
    }
    let header = document.createElement("h2");
 //   if (alternativOrganisationIDs.length != 0){
 //       let header_content = document.createTextNode("Zusätzlich diese Kalender Anzeigen:");
 //       header.appendChild(header_content);
 //   }

    let form = document.createElement("form");
    form.setAttribute("id", "snk-stammesAuswahl");
    form.appendChild(header);

    for (const alternativOrganisationID of alternativOrganisationIDs){
        organisation = organisations.find(e => parseInt(e.global_id) == parseInt(alternativOrganisationID));
        let div = document.createElement("div");
        div.setAttribute("class", "snkOptionalStructure");
        div.setAttribute("onclick", "toggelSelect("+alternativOrganisationID+");");
        let imgdiv = document.createElement("div");
        imgdiv.setAttribute("class", "checkbox");
        let img = document.createElement("img");
        if ( aktivCalendars.includes(parseInt(alternativOrganisationID))){
            img.setAttribute("alt", "Kalender von "+organisation.name+" aktiviert");
            img.setAttribute("src","../img/calendar/VCP_Waldlauferzeichen_Feuergelb_Ja.png");
        } else {
            img.setAttribute("alt", "Kalender von "+organisation.name+" deaktiviert");
            img.setAttribute("src","../img/calendar/VCP_Waldlauferzeichen_Feuergelb_Nein.png");
        }
        img.setAttribute("height", "25");
        img.setAttribute("width", "25");
        imgdiv.appendChild(img);
        div.appendChild(imgdiv);
        let logodiv = document.createElement("div");
        logodiv.setAttribute("class", "logo");

        if(typeof organisationLogos[parseInt(alternativOrganisationID)] === "undefined") {
            console.log(organisation.name+" Logo is undefined");
        } else {
            let logoimg = document.createElement("img");
            logoimg.setAttribute("alt", "Logo von "+organisation.name);
            logoimg.setAttribute("height", "20");
            logoimg.setAttribute("width", "31");
            logoimg.setAttribute("src", organisationLogos[parseInt(alternativOrganisationID)]);
            logodiv.appendChild(logoimg);
        }
        div.appendChild(logodiv);
        let lable = document.createElement("label");
        lable.setAttribute("for", "Button zum aktivieren von "+organisation.name+" Terminen");
        let lable_content = document.createTextNode(organisation.name);
        lable.appendChild(lable_content);
        div.appendChild(lable);
        form.appendChild(div);
    }
    terminForm.replaceChildren(form);
}


async function main() {
    let div = document.createElement("div");
    div.setAttribute("class", "inner");
    let div_w = document.createElement("div");
    div_w.setAttribute("class", "vcpwlz text-vcpblau");
    div_w.setAttribute("style", "font-size: 4em;");
    let div_w_content = document.createTextNode("G");
    div_w.appendChild(div_w_content);
    div.appendChild(div_w);
    let p = document.createElement("p");
    let p_content = document.createTextNode("Die Termine werden geladen... Einen Moment bitte.");
    p.appendChild(p_content);
    div.appendChild(p);
    terminDatenschutz.replaceChildren(div);

    try{
	await fetchAllData();
        buildCalendarForm();
	buildList();
        terminDatenschutz.remove();
    } catch(e){
        console.log('Error: '+e);
		
        let div = document.createElement("div");
        div.setAttribute("class", "inner");
        let div_w = document.createElement("div");
        div_w.setAttribute("class", "vcpwlz text-feuerrot");
        div_w.setAttribute("style", "font-size: 4em;");
        let div_w_content = document.createTextNode("S");
        div_w.appendChild(div_w_content);
        div.appendChild(div_w);
        let p = document.createElement("p");
        let p_content = document.createTextNode("Hier ist etwas schief gelaufen. Entschuldigung! Bitte versuche es später noch ein mal.");
        p.appendChild(p_content);
        div.appendChild(p);
        terminDatenschutz.replaceChildren(div);

    } 
}
</script>
