# Impressum

### Angaben gemäß § 5 TMG

    VCP Stammesname 
    c/o VCP Hessen e.V.
    Johannisberg 12
    61231 Bad Nauheim

E-Mail: <a href="javascript:linkTo_UnCryptMailto(hierIstEinKomplizierterHaswert);">ungueltigeEmail.de</a>


### Redaktionell verantwortlich

    VCP Stammesname
    Straße Hausnummer von Gruppenraum 
    PLZ Ort

### Schlichtungsstelle
Wir sind nicht bereit oder verpflichtet, an Streitbeilegungsverfahren vor einer
Verbraucherschlichtungsstelle teilzunehmen.


# Datenschutzerklärung 
Mit dem Besuch dieser Website werden je nach Aktion auf der Website wie im Folgenden beschrieben personenbezogene Daten zu unterschiedlichen Zwecken und mit unterschiedlichen rechtlichen Grundlagen verarbeitet.

Die Seite ist SSL verschlüsselt, wodurch die sichere Datenübertragung zu dieser Seite generiert wird und keine der Daten, die Sie uns zur Verfügung stellen, von Dritten mitgelesen werden können. Zu erkennen ist die Verschlüsselung an der Adresszeile des Browsers https:// und an dem Schloss-Symbol.

## Log-Files

Bei einem Besuch auf dieser Webseite werden durch den verwendeten Browser auf ihrem Endgerät personenbezogenen Daten teils anonymisiert in so genannten Log-Files auf dem Server unserer Website verarbeitet. Dies dient nach §6 Abs. 8 DSG-EKD der Wahrung unseres Berechtigen Interesses, die Auslieferung der Website an Ihr Endgerät zu ermöglichen und die Funktionsfähigkeit sicherzustellen. Dies ist auch gleichzeitig der Zweck der Verarbeitung der personenbezogenen Daten. Folgende personenbezogene Daten werden verarbeitet:

- IP-Adresse des anfragenden Rechners (anonymisiert)
- Datum und Uhrzeit des Zugriffs
- Name und URL der abgerufenen Datei
- Website, von der aus der Zugriff erfolgt (Referrer-URL),
- verwendeter Browser und ggf. das Betriebssystem Ihres Rechners sowie der Name Ihres Access-Providers

Um die Sicherheit ihrer Daten auch auf dem Server unserer Website sicherzustellen, haben wir mit unserem Hoster-Dienstleister gemäß §30 DSG-EKD einen Vertrag zur Auftragsverarbeitung abgeschlossen.

## Cookies
Internetseiten verwenden teilweise so genannte Cookies. Diese richten auf Ihrem Rechner keinen Schaden an und enthalten keine Viren. Cookies dienen dazu, unser Angebot nutzerfreundlicher, effektiver und sicherer zu machen. Cookies sind kleine Textdateien, die Ihr Browser speichert und auf Ihrem Rechner ablegt.

Die meisten der von uns verwendeten Cookies sind so genannte „Session-Cookies“. Sie werden nach Ende Ihres Besuchs automatisch gelöscht. Andere Cookies bleiben auf Ihrem Endgerät gespeichert, bis Sie diese löschen. Diese Cookies dienen unserem berechtigten Interesse (§6 Abs. 8 DSG-EKD), die Nutzung und Bereitstellung unserer Website für Sie zu ermöglichen, denn diese Cookies ermöglichen es uns, Ihren Browser beim nächsten Besuch wiederzuerkennen und sind damit für die Nutzung der Website technisch notwendig.

Sie können Ihren Browser so einstellen, dass Sie über das Setzen von Cookies informiert werden und Cookies nur im Einzelfall erlauben, die Annahme von Cookies für bestimmte Fälle oder generell ausschließen sowie das automatische Löschen der Cookies beim Schließen des Browser aktivieren. Bei der Deaktivierung von Cookies kann die Funktionalität dieser Website eingeschränkt sein.

## Einbindung anderer Websites
### Twitter
Die Website enthält eine Verlinkung zu Twitter der Twitter Inc. (1355 Market St, Suite 900, San Francisco, California 94103, USA), die jedoch nur durch aktives Anklicken auf entsprechende Website weiterleitet und ohne ein anklicken keine Daten weitergibt. Die entsprechende Datenschutzerklärung der Website kann unter folgendem Link eingesehen werden: [https://twitter.com/de/privacy](https://twitter.com/de/privacy)
### Facebook
Die Website enthält eine Verlinkung zu Facebook der Meta Platforms Inc. (Menlo Park, California 94103, USA), die jedoch nur durch aktives Anklicken auf entsprechende Website weiterleitet und ohne ein anklicken keine Daten weitergibt. Die entsprechende Datenschutzerklärung der Website kann unter folgendem Link eingesehen werden: [https://www.facebook.com/legal/terms?ref=pf](https://www.facebook.com/legal/terms?ref=pf)
### Instagram
Die Website enthält eine Verlinkung zu Instagram der Meta Platforms Inc. (Menlo Park, California 94103, USA), die jedoch nur durch aktives Anklicken auf entsprechende Website weiterleitet und ohne ein anklicken keine Daten weitergibt. Die entsprechende Datenschutzerklärung der Website kann unter folgendem Link eingesehen werden: [https://help.instagram.com/581066165581870](https://help.instagram.com/581066165581870)
### Youtube
Die Website enthält eine Verlinkung zu Youtube der Alphabet Inc. (Mountain View, California 94103, USA), die jedoch nur durch aktives Anklicken auf entsprechende Website weiterleitet und ohne ein anklicken keine Daten weitergibt. Die entsprechende Datenschutzerklärung der Website kann unter folgendem Link eingesehen werden: [https://policies.google.com/privacy?hl=de](https://policies.google.com/privacy?hl=de)
### ScoutNet
Die Website enthält eine Verlinkung zu ScoutNet der ScoutNet e. V. (Richterstraße 22, 91052 Erlangen), die jedoch nur durch aktives Anklicken auf entsprechende Website weiterleitet und ohne ein anklicken keine Daten weitergibt. Die entsprechende Datenschutzerklärung der Website kann unter folgendem Link eingesehen werden: [https://www.scoutnet.de/datenschutz](https://www.scoutnet.de/datenschutz)

## Rechte von betroffenen Personen

Nach den §§19-22 DSG-EKD und 24-25 DSG-EKD bzw. §46 DSG-EKD haben Sie als betroffene Person die folgende Rechte:

- Auskunftsrecht der betroffenen Person §19 DSG-EKD

Sie haben das Recht zu erfahren, welche ihrer personenbezogenen Daten und Kategorien von personenbezogenen Daten zu welchem Zweck, auf welcher Rechtsgrundlage, bei welchen Empfängern und für welche Dauer von uns verarbeitet werden.

- Recht auf Berichtigung §20 DSG-EKD

Sie haben das Recht die Berichtigung Ihrer bei uns verarbeiteten personenbezogenen Daten zu verlangen.

- Recht auf Löschung §21 DSG-EKD

Sie haben das Recht auf Löschung Ihrer bei uns verarbeiteten personenbezogenen Daten, wenn die Verarbeitung keinen rechtsgültigen Zweck erfüllt oder unzulässig ist. Ist die Verarbeitung zur Ausübung des Rechts auf freie Meinungsäußerung und Information, zur Erfüllung einer rechtlichen Verpflichtung, aus Gründen des öffentlichen Interesses, für im kirchlichen Interesse liegende Archivzwecke, wissenschaftliche oder historische Forschungszwecke, für statistische Zwecke oder zur Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen erforderlich, müssen die Daten nicht gelöscht werden.

- Recht auf Einschränkung der Verarbeitung §22 DSG-EKD

Sie haben das Recht die Einschränkung der von Ihnen bei uns verarbeiteten personenbezogenen Daten zu verlangen, wenn Sie die Richtigkeit der Daten bestreiten, wenn die Verarbeitung unrechtmäßig ist und Sie statt einer Löschung eine Einschränkung verlangen, wir die Daten nicht mehr benötigen, Sie jedoch diese zur Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen benötigen oder Sie gemäß §25 DSG-EKD Widerspruch gegen die Verarbeitung eingelegt haben.

- Recht auf Datenübertragbarkeit §24 DSG-EKD

Wenn die Verarbeitung Ihrer personenbezogenen Daten bei uns auf einem Vertrag beruht und mit Hilfe automatisierter Verfahren erfolgt ist, haben Sie das Recht Ihre verarbeiteten Daten in einem strukturierten, gängigen und maschinenlesbaren Format zu erhalten oder die Übermittlung an einen anderen Dritten zu verlangen. Das Recht auf Datenübertragbarkeit gilt nicht für eine Verarbeitung, die für die Wahrnehmung einer Aufgabe erforderlich ist, die im kirchlichen Interesse liegt oder in Ausübung kirchlicher Aufsicht erfolgt, die der kirchlichen Stelle übertragen wurde.

- Recht auf Widerspruch §25 DSG-EKD

Wenn kein zwingendes kirchliches Interesse an der Verarbeitung besteht, das Interesse einer dritten Person nicht überwiegt und keine Rechtsvorschrift zur Verarbeitung verpflichtet, haben Sie das Recht Widerspruch gegen die Verarbeitung Ihrer personenbezogenen Daten einzulegen.

- Recht auf Beschwerde §46 DSG-EKD

Sie haben das Recht sich bei einer Aufsichtsbehörde über die Verarbeitung Ihrer personenbezogenen Daten zu beschweren.

